# React and Node Template + Tailwind Css

## Get Started

### `npm run postinstall`

Installs both sides dependencies (client, server)

## Available Scripts

In the project directory, you can run:

### `npm run client`

Runs the React font-end app in the development mode.

### `npm run server`

Runs the Express back-end server in the development mode.

### `npm run build`

Builds the React font-end app for production to the `build` folder.\
