import React, {useState} from 'react'
import logo from './logo.svg';
import axios from 'axios'
import './App.css';

const App= ()=> {
const [data, setData] = useState('')
  async function getData(){
    const data = await axios.get('/test')
    console.log(data.data.data)
    setData(data.data.data)
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <button onClick={getData} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">
          Button
        </button>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          {data}
        </a>
      </header>
    </div>
  );
}

export default App;
