const express = require('express');
const bodyParser = require('body-parser');
const path = require('path')
const cors = require('cors')
require('dotenv').config()


const app = express()

app.use(cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.set('port', process.env.PORT || 3002)

app.use('/', require('./contoller'))


app.listen(app.get('port'), ()=> {
    console.log('Server listening on port', app.get('port'))
})