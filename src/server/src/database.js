const mysql = require('mysql')
require('dotenv').config()

const pool = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
});

pool.getConnection(err => {
    if (err) {
        console.log('Error connecting to db...');
    } else {
        console.log('Connected to db...');
    }
});

const executeQuery = (query, arrayParams) => {
    return new Promise((resolve, reject) => {
        try {
            pool.query(query, arrayParams, (err, data) => {
                if (err) {
                    console.log('Error executing query...');
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        } catch (err) {
            reject(err);
        }
    });
};

module.exports = executeQuery